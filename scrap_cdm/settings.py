BOT_NAME = 'scrap_cdm'

SPIDER_MODULES = ['scrap_cdm.spiders']
NEWSPIDER_MODULE = 'scrap_cdm.spiders'

USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"

ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 32

DOWNLOADER_MIDDLEWARES = {
    'scrap_cdm.middlewares.scrapCDMDownloaderMiddleware': 543,
}
ITEM_PIPELINES = {
    'scrap_cdm.pipelines.scrapCDMPipeline': 300,
}

